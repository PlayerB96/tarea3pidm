package com.example.tarea2pidm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lstpersonas;
    ArrayList<String> nombres;
    ArrayList<Persona> listapersonas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lstpersonas = (ListView) findViewById(R.id.LSTPERSONAS);

            listapersonas = new ArrayList<Persona>();
            Persona objIvanPetrlik = new Persona();
                    objIvanPetrlik.setNombre("Ivan");
                    objIvanPetrlik.setApellido("Petrlik");
            Persona objBryanPetrlik = new Persona();
                    objBryanPetrlik.setNombre("Bryan");
                    objBryanPetrlik.setApellido("Petrlik");

                    listapersonas.add(objIvanPetrlik);
                    listapersonas.add(objBryanPetrlik);



        /*
        nombres = new ArrayList<String>();
        nombres.add("IVAN PETRSLIK");
        nombres.add("BRYAN PETRSLIK");
        nombres.add("MATOS PETRSLIK");
        nombres.add("OMAR PETRSLIK");
        */
        /*
        ArrayAdapter<String> adapter = new ArrayAdapter<String>( this,
                android.R.layout.simple_list_item_1, nombres);

        lstpersonas.setAdapter(adapter);
        */
        /*
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, nombres);
        */
        lstpersonas.setAdapter(new MyAdapter(this,listapersonas));

        lstpersonas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
            {
                Toast.makeText(MainActivity.this, "Has pulsado: " + listapersonas.get(position).getNombre()
                                +" "+listapersonas.get(position).getApellido(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}