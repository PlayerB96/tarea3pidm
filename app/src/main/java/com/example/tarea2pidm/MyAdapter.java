package com.example.tarea2pidm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
public class MyAdapter extends BaseAdapter
{
    private Context context;
    private int layout;
    private ArrayList<Persona> nombres;

    public MyAdapter(Context context, ArrayList<Persona> nombres)
    {
        this.context = context;
        this.nombres = nombres;
    }
    @Override
    public int getCount() {
        return this.nombres.size();
    }

    @Override
    public Object getItem(int position) {
        return this.nombres.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    static class Controles {
        TextView lblnombre;
        TextView lblapellido;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        Controles controles;

        if(convertView == null){
            LayoutInflater layoutInflater = LayoutInflater.from(this.context);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
            controles = new Controles();
            controles.lblnombre=(TextView) convertView.findViewById(R.id.lblnombre);
            controles.lblapellido=(TextView) convertView.findViewById(R.id.lblapellido);
            convertView.setTag(controles);
        }
        else {
            controles = (Controles) convertView.getTag();
        }

        controles.lblnombre.setText(nombres.get(position).getNombre());
        controles.lblapellido.setText(nombres.get(position).getApellido());

        return convertView;
        /*
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.list_item, null);
        String currentName = nombres.get(position);
        TextView textView = (TextView) v.findViewById(R.id.lblnombre);
        textView.setText(currentName);
        return v;

        */
    }
}
